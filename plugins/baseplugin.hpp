#ifndef BASEPLUGIN_HPP
#define BASEPLUGIN_HPP

#include "halconProcessorInterface.hpp"

class Request1:public HalconProcessorInterface {
    uint getId(){
        return 1;
    }

    std::string getDescription() {
        return "Calculate difference of center of circle of big one and small one."
               "If difference is more than 0.15mm, mark it as wrong";
    }

    void processImage(const HalconCpp::HObject& ho_image, HalconResult& msg);

};
/////////////////////////////////////////////////////////
/// An EagleProvider is a WarriorProvider,
/// provide warriors of type Eagle
/////////////////////////////////////////////////////////
PLUMA_INHERIT_PROVIDER(Request1, HalconProcessorInterface);

#endif // BASEPLUGIN_HPP
